Download 
========

MARE2DEM is hosted on bitbucket.org and can be `downloaded at
https://bitbucket.org/mare2dem <https://bitbucket.org/mare2dem>`_.

There MARE2DEM code is split across three repositories:

- mare2dem_source 
- mare2dem_matlab 
- mare2dem_examples

The recommended way to get the code is to use ``git clone`` to clone
each repository to your local machine. This way, whenever one of the
repositories is updated, you can use ``git pull`` to incrementally update your local
copy without requiring a full download and rebuilding of the code.  

.. code-block:: bash

    git clone https://bitbucket.org/mare2dem/mare2dem_source.git
    git clone https://bitbucket.org/mare2dem/mare2dem_matlab.git
    git clone https://bitbucket.org/mare2dem/mare2dem_examples.git
 
Don't know what git is? Learn more `here <https://www.atlassian.com/git/tutorials>`_.


